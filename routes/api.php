<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('auth/register', 'UserController@register');
Route::post('auth/login', 'UserController@login');
Route::post('auth/forgetPW', 'UserController@ForgetPW');
Route::get('user', 'UserController@getUser')->middleware('auth:api');
Route::get('auth/logout','UserController@destroy')->middleware('auth:api');
Route::post('auth/checkPassword','UserController@checkPassword')->middleware('auth:api');
Route::post('auth/changePassword','UserController@changePassword')->middleware('auth:api');
Route::post('editUser','UserController@editUser')->middleware('auth:api');
Route::post('auth/setChangePW','UserController@setChangePW');
Route::post('auth/resetPW','UserController@resetPW');

Route::get('test', 'GoogleVisionController@test');

