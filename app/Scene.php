<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scene extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'story_id','name','number','image_path'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function story (){
        return $this->belongsTo(Story::class);
    }
}
