<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SceneTag extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'scene_id', 'tag_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function scene (){
        return $this->belongsTo(Scene::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tag (){
        return $this->belongsTo(Tag::class);
    }
}
