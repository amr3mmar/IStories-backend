<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class Welcome
 * @package App\Mail
 */
class ForgetPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $code=0;
    public $id=0;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($code, $id)
    {
        $this->code=$code;
        $this->id=$id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.forgetPW');
    }
}
