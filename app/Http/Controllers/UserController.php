<?php
namespace App\Http\Controllers;
use App\Mail\TaskInvitation;
use App\Task;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use App\User;
use App\Mail\Welcome;
use App\Mail\ForgetPassword;
use JWTAuthException;
use Auth;
use Socialite;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Validator;


/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @var User
     */
    private $user;
    /**
     * UserController constructor.
     * @param User $user
     */
    public function __construct(User $user){
        $this->user = $user;
    }

    /**
     * registers a new user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request){
        $validator = Validator::make($request->all(),[
            'name'=>'required|string|max:255',
            'email'=>'required|string|max:255|email|unique:users',
            'password'=>'required|string|min:6',
        ]);
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],400);
        }

        $user =User::where('email',$request->get('email'))->first();
        if($user){
            return response()->json(['status'=>true,'message'=>'Email Already Exists'],400);
        }

        $user = $this->user->create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password'))
        ]);
        \Mail::to($user)->send(new Welcome);
       return response()->json(['status'=>true,'message'=>'User created successfully','data'=>$user],200);
    }

    /**
     * logs in with a user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
        
        $credentials = $request->only('email', 'password');
        $token = null;
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['status' => false, 'message' => "Invalid email or password"],400);
            }
        } catch (JWTAuthException $e) {
            return response()->json(['message'=>"failed_to_create_token"], 500);
        }
        return response()->json(compact('token'));
    }

    /**
     * gets the logged in user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAuthUser(Request $request){
        $user = JWTAuth::toUser($request->token);
        return response()->json(['result' => $user]);
    }

    /**
     * sends an email for password resetting
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ForgetPW(Request $request){
        $validator = Validator::make($request->all(),[
            'email'=>'required|email'
        ]);
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],400);
        }

        $code=str_random(8);

        $user = User::where('email', $request->get('email'))->first();
        $user->code=$code;
        $user->save();
        if($user){
            \Mail::to($user)->send(new ForgetPassword($code, $user->id));
        }else{
            return response()->json(['status' => false, 'message' => "User not found"],400);
        }

    }

    public function getUser(){
        $user= User::find(auth()->id());
        return response()->json(['status'=>true,'message'=>'user found','data'=>$user]);
    }

    public function setChangePW(Request $request){
        $user = User::find($request->get('id'))
                ->where('code', $request->get('code'))
                ->first();
        if($user){
            if($request->get('code')){
            return response()->json(['status'=>true,'message'=>"Enter your new password"],200);
            }
        }
        return response()->json(['status'=>false,'message'=>"Incorrect Code"],400);
    }

    /**
     * changes the user's data
     * @return \Illuminate\Http\JsonResponse
     */
    public function editUser(Request $request){
        $validator = Validator::make($request->all(),[
            'name'=>'required'
        ]);
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],400);
        }

        $user = User::find(['id',auth()->id()])->first();
        $input = $request->all();
        $user->fill($input)->save();
        return response()->json(['status'=>true,'message'=>'User updated successfully', 'data'=>$user],200);
    }

    /**
     * logouts a user
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(){
        JWTAuth::invalidate(JWTAuth::getToken());
        return response()->json(['status' => false, 'message' => "You are now logged out"],200);
    }

    /**
     * checks the user's password
     * @request('new_password')
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request){
        $validator = Validator::make($request->all(),[
            'old_password'=>'required',
            'new_password'=>'required|string|min:6',
            'confirm_password'=>'required'
        ]);
        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()],400);
        }
        $user=User::find(auth()->id());
        $userpassword= $user->password;
        if(password_verify ( $request->get('old_password') , $userpassword )){
            if($request->get('new_password')==$request->get('confirm_password')){
                $user->password=bcrypt(request('new_password'));
                $user->save();
                return response()->json(['status' => true, 'message' => "Password Updated Successfully"],200);
            }else{
                return response()->json(['status' => false, 'message' => "passwords didn't match"],400);
            }

        }else{
            return response()->json(['status' => false, 'message' => "Incorrect Password"],400);
        }
    }

    /**
     * resets the user's password
     * @request('new_password')
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPW(Request $request){

        $user = User::find($request->get('id'))
                ->where('code', $request->get('code'))
                ->first();
        if($user){

             $validator = Validator::make($request->all(),[
                'new_password'=>'required|string|min:6'
            ]);
            if($validator->fails()){
                return response()->json(['errors'=>$validator->errors()],400);
            }

            if($request->get('new_password')==$request->get('confirm_password')){
                $user->password=bcrypt(request('new_password'));
                $user->code=null;
                $user->save();
                return response()->json(['status' => true, 'message' => "Password Updated Successfully"],200);
            }else{
                return response()->json(['status' => false, 'message' => "passwords didn't match"],400);
            }
        }else{
        return response()->json(['status'=>false,'message'=>"Incorrect Code"],400);
        }
    }
}