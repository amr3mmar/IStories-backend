<?php

namespace App\Http\Controllers;

use App\SceneTag;
use Illuminate\Http\Request;

class SceneTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SceneTag  $sceneTag
     * @return \Illuminate\Http\Response
     */
    public function show(SceneTag $sceneTag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SceneTag  $sceneTag
     * @return \Illuminate\Http\Response
     */
    public function edit(SceneTag $sceneTag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SceneTag  $sceneTag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SceneTag $sceneTag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SceneTag  $sceneTag
     * @return \Illuminate\Http\Response
     */
    public function destroy(SceneTag $sceneTag)
    {
        //
    }
}
