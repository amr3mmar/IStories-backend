<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

 # Includes the autoloader for libraries installed with composer
 require __DIR__ . '../../../vendor/autoload.php';

 # Imports the Google Cloud client library
 use Google\Cloud\Vision\VisionClient;

class GoogleVisionController extends Controller
{
    public function test(){
         # Your Google Cloud Platform project ID
        $projectId = 'lithe-climber-197021';

        # Instantiates a client
        $vision = new VisionClient([
            'projectId' => $projectId
        ]);

        # The name of the image file to annotate
        $fileName = '../../../public/uploads/images/insignia.jpg';

        # Prepare the image to be annotated
        $image = $vision->image(fopen($fileName, 'r'), [
            'LABEL_DETECTION'
        ]);

        # Performs label detection on the image file
        $labels = $vision->annotate($image)->labels();
        
        return response()->json(['status'=>true,'labels'=>$labels],200);

    }
}
