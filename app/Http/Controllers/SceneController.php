<?php

namespace App\Http\Controllers;

use App\Scene;
use Illuminate\Http\Request;

class SceneController extends Controller
{
    public function uploadImage(Request $request){

        $user = User::find(['id', auth()->id()])->first();
        if ($user->is_admin) {
            $validator = Validator::make($request->all(),[
                'story_id'=>'required',
                'name'=>'required',
                'number'=>'required|integer',
                'file'=>'required'
            ]);
            if($validator->fails()){
                return response()->json(['errors'=>$validator->errors()],400);
            }

            $file=$request->file('file');
            $image_name = $file->getClientOriginalName();
            $destinationPath = 'uploads/images';

            Scene::create([
                'story_id'=>$request->get('story_id'),
                'name'=>$image_name,
                'number'=>$request->get('number'),
                'image_path'=>$destinationPath.'/'.$image_name
            ]);
            $file->move($destinationPath,$file->getClientOriginalName());
            return response()->json(['status'=>true,'message'=>'File uploaded successfully'],200);
        }
        else {
            return response()->json(['status'=>true,'message'=>'You are not authorized'],400);
        }
    }
    public function deleteImage(Request $request)
    {
        $user = User::find(['id', auth()->id()])->first();
        if ($user->is_admin) {
            $validator = Validator::make($request->all(),[
                'scene_id'=>'required'
            ]);
            if($validator->fails()){
                return response()->json(['errors'=>$validator->errors()],400);
            }

            $scene = Scene::findOrFail($request->get('scene_id'));
            $scene->delete();
            return response()->json(['status'=>true,'message'=>'File deleted successfully'],200);

        }
        else{
            return response()->json(['status'=>true,'message'=>'You are not authorized'],400);
        }
    }
    public function getImages(){
        $images = File::all();
        return response()->json(['status'=>true,'data'=>$images],200);
    }
}
